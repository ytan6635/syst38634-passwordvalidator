
/*
 * Yi Tan
 * stn: 991554680
 */



package passwordValidator;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {
	


	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.IsValidLength("1234567890");
		assertTrue("Invalid Length", result);
		
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.IsValidLength("");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean result = PasswordValidator.IsValidLength("  test  ");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.IsValidLength("12345678");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.IsValidLength("1234567");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsRegular() {
		boolean result = PasswordValidator.IsValidLength("12345678910");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsException() {
		boolean result = PasswordValidator.IsValidLength("");
		assertFalse("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryIn() {
		boolean result = PasswordValidator.IsValidLength("abcdefghg12");
		assertTrue("Invalid Length", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryOut() {
		boolean result = PasswordValidator.IsValidLength("abcdefghg1");
		assertTrue("Invalid Length", result);
	}

	

}
	

