/*
 * Yi Tan
 * stn: 991554680
 * 
 * validates passwords and is being developed using TDD 
 */

package passwordValidator;

public class PasswordValidator {


		// TODO Auto-generated constructor stub
		private static int MIN_LENGTH=8;
		
		/**
		 * 
		 *
		 * @param password
		 * @return true if the number of characters is 8 or more. No spaces are allowed
		 */
		public static void main(String[] args) {
			
			System.out.println(IsValidateDigits("abcdefghg23"));
			
		}
		
		public static boolean IsValidLength(String password) {		
			if(password.indexOf(" ")>=0) {
				return false;
			}

			return  password.trim().length()>=MIN_LENGTH;
			
		}

/*
 * @param password
 * @return true if the number of digits is 2 or more and the number of characters is 8 or more. 
 */
	
		public static boolean IsValidateDigits(String password) {
			if(password.length()<8) {
				return false;
			}
		
			int num =0; 

			for(char c:password.toCharArray()) {
				if(Character.isDigit(c)) {
					num++;
				}
			}
			
		
			return num>=2?true:false;
		

		

	}
		

}
